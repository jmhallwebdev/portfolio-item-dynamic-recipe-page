<?php
echo "<script type='text/javascript'>
        jQuery(document).ready(function(){
            jQuery('#hideshow').on('click', function(event) {        
            jQuery('#preparationRow').toggle('show');
        });
    });
            jQuery(document).ready(function(){
            jQuery('#hideshow2').on('click', function(event) {        
            jQuery('#preparationRow').toggle('show');
        });
    });
            jQuery(document).ready(function(){
            jQuery('#hideshow3').on('click', function(event) {        
            jQuery('#preparationRow').toggle('show');
        });
    });
        
        function GetSelectedValue(ourSelect) {
        var selectedValue = ourSelect.value;

        if (selectedValue == 'crockpotchili') {
            $('#ourDescription').load('crockpotchili.php');
            }

        if (selectedValue == 'tacochili') {
            $('#ourDescription').load('tacochili.php');
            }

        if (selectedValue == 'pheasantchili') {
            $('#ourDescription').load('pheasantchili.php');
            }  
       }

       function addIngredPrepRowsNormal() {
            $(ingredientsRowHalf).css('display', 'none');
            $(ingredientsRowDouble).css('display', 'none');
            $(ingredientsRowNormal).css('display', 'inherit');
            }

        function addIngredPrepRowsHalf() {
            $(ingredientsRowNormal).css('display', 'none');
            $(ingredientsRowDouble).css('display', 'none');
            $(ingredientsRowHalf).css('display', 'inherit');
            }

        function addIngredPrepRowsDouble() {
            $(ingredientsRowNormal).css('display', 'none');
            $(ingredientsRowHalf).css('display', 'none');
            $(ingredientsRowDouble).css('display', 'inherit');
            }

        function closeAllPrep() {
            $(preparationRow).css('display', 'none');
            }

    </script>
    <script src='js/jquery-3.1.0.js'></script>
    <script src='js/bootstrap.min.js'></script>

    <div class='row'>
        <div class='col-lg-12'>
            <h1 class='page-header'>PHEASANT CHILI
                <small>Yummy!</small>
            </h1>
        </div>
    </div>
    <!-- /.row -->

    <!-- Portfolio Item Row -->
    <div class='row'>

        <div class='col-md-8'>
            <img class='img-responsive' src='images/pheasantchili.jpg' alt='pheasant chili picture'>
        </div>

        <div class='col-md-4 jerbold'>
            <h3>A Fresh Alternative!</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
            <h3>WHAT YOU NEED TO KNOW</h3>
            <ul>
                <li>Serves: 6</li>
                <li>Prep Time: 25 min</li>
                <li>Cooking Time: 45 min</li>
            </ul>
        </div>

    </div>
    <!-- /.row -->

    <div class='row'>

        <div class='col-md-6 batchDiv'>
            <h3>CHOOSE YOUR BATCH SIZE:</h3>
        </div>

        <div class='col-md-6 batchDiv'>
            <input type='radio' name='batch' value='regular' id='regular' class='hvr-grow' onclick='addIngredPrepRowsNormal()'> Regular 6 serving batch<br><br>
            <input type='radio' name='batch' value='half' id='half' class='hvr-grow' onclick='addIngredPrepRowsHalf()'> Half sized 3 serving batch<br><br>
            <input type='radio' name='batch' value='double' id='double' class='hvr-grow' onclick='addIngredPrepRowsDouble()'> Double sized: 12 serving batch<br><br>
        </div>

    </div>
    <!-- /.row -->

    <!-- Ingredients List Row -->
    <div class='row animated fadeIn' id='ingredientsRowNormal'>

        <div class='col-lg-12'>
            <h3 class='page-header'>NORMAL BATCH INGREDIENTS LIST</h3>
        </div>

        <div class='col-sm-6 col-xs-12'>
            <p>
                <ul>
                    <li>1/2 cup olive oil
                    <li>4 15 oz cans cannelini beans </li>
                    <li>1 15 oz can kidney beans </li>
                    <li>3 bell peppers, one each of red, yellow and orange, diced </li>
                    <li>2 large diced onions </li>
                    <li>3 dried ancho peppers, rehydrated and diced</li>
                    <li>1 quarter cup chopped garlic </li>
                    <li>1/2 cup sliced celery </li>
                    <li>2 quarts chicken or pheasant stock </li>
                    <li>1 pint whole whipping cream </li>
                    <li>1 1/2 cups flour </li>
                    <li>Pheasant breasts of two pheasants, cubed </li>
                    <li>2 fresh bay leaves</li> 
                    <li>1 1/2 tbsp chili powder</li>
                    <li>1 1/2 tbsp cumin</li>
                    <li>Salt & pepper to taste</li>
                    <li>Nacho Chips</li>
                    <li>Cilantro</li>
                </ul>
            </p>
            <input type='button' id='hideshow' value='Show / Hide Preparation Instructions'>
        </div>

        <div class='col-sm-6 col-xs-12'>
            <img class='img-responsive portfolio-item' src='images/veggies7.jpg' alt='fresh ingredients picture'>
        </div>

    </div>
    <!-- /.row -->

    <!-- Ingredients List Row -->
    <div class='row animated fadeIn' id='ingredientsRowHalf'>

        <div class='col-lg-12'>
            <h3 class='page-header'>HALF BATCH INGREDIENTS LIST</h3>
        </div>

        <div class='col-sm-6 col-xs-12'>
            <p>
                <ul>
                    <li>1/4 cup olive oil
                    <li>2 15 oz cans cannelini beans </li>
                    <li>1/2 15 oz can kidney beans </li>
                    <li>2 bell peppers, diced </li>
                    <li>1 large diced onions </li>
                    <li>1/2 dried ancho peppers, rehydrated and diced</li>
                    <li>1/2 quarter cup chopped garlic </li>
                    <li>1/4 cup sliced celery </li>
                    <li>1 quart chicken or pheasant stock </li>
                    <li>1/2 pint whole whipping cream </li>
                    <li>3/4 cup flour </li>
                    <li>Pheasant breast of 1 pheasants, cubed </li>
                    <li>1 fresh bay leaves</li> 
                    <li>3/4 tbsp chili powder</li>
                    <li>3/4 tbsp cumin</li>
                    <li>Salt & pepper to taste</li>
                    <li>Nacho Chips</li>
                    <li>Cilantro</li>
                </ul>
            </p>
            <input type='button' id='hideshow2' value='Show/Hide Preparation Instructions'>
        </div>

        <div class='col-sm-6 col-xs-12'>
            <img class='img-responsive portfolio-item' src='images/veggies8.jpg' alt='fresh ingredients picture'>
        </div>

    </div>
    <!-- /.row -->

    <!-- Ingredients List Row -->
    <div class='row animated fadeIn' id='ingredientsRowDouble'>

        <div class='col-lg-12'>
            <h3 class='page-header'>DOUBLE BATCH INGREDIENTS LIST</h3>
        </div>

        <div class='col-sm-6 col-xs-12'>
            <p>
                <ul>
                    <li>4 tbsp. cooking oil
                    <li>1 cup olive oil
                    <li>8 15 oz cans cannelini beans </li>
                    <li>2 15 oz can kidney beans </li>
                    <li>6 bell peppers, 2 each of red, yellow and orange, diced </li>
                    <li>4 large diced onions </li>
                    <li>6 dried ancho peppers, rehydrated and diced</li>
                    <li>1/2 cup chopped garlic </li>
                    <li>1 cup sliced celery </li>
                    <li>1 gallon chicken or pheasant stock </li>
                    <li>2 pints whole whipping cream </li>
                    <li>3 cups flour </li>
                    <li>Pheasant breasts of 4 pheasants, cubed </li>
                    <li>4 fresh bay leaves</li> 
                    <li>3 tbsp chili powder</li>
                    <li>3 tbsp cumin</li>
                    <li>Salt & pepper to taste</li>
                    <li>Nacho Chips</li>
                    <li>Cilantro</li>
                </ul>
            </p>
            <input type='button' id='hideshow3' value='Show/Hide Preparation Instructions'>
        </div>

        <div class='col-sm-6 col-xs-12'>
            <img class='img-responsive portfolio-item' src='images/veggies9.jpg' alt='fresh ingredients picture'>
        </div>

    </div>
    <!-- /.row -->

    <!-- Preparations Row -->
    <div class='row animated fadeIn' id='preparationRow'>

        <div class='col-lg-12'>
            <h3 class='page-header'>PREPARATION INSTRUCTIONS</h3>
        </div>

        <div class='col-sm-6 col-xs-12'>
            <p>
                <ul>
                    <li>Heat a stock pot until it is very hot, then add olive oil and quickly brown the pheasant pieces. .</li>
                    <li>Add cumin and chili powder, cook a minute or two, then add the garlic.</li>
                    <li>Add peppers, onion and celery, then stir.</li>
                    <li>Add two fresh bay leaves, the flour, and then stir.</li>
                    <li>Add the pheasant or chicken stock, beans and whipping cream, then stir.</li>
                    <li>Add the ancho chili, and simmer for 45 minutes.</li>
                    <li>Finally, add salt and white pepper to taste.</li>
                    <li>Garnish with nacho chips and a bit of cilantro.</li>
                </ul>
            </p>
        </div>
        <div class='col-sm-6 col-xs-12'>
            <img class='img-responsive portfolio-item' src='images/pheasantchili2.jpg' alt='chili bowl picture'>
        </div>
    </div>"
    ?>